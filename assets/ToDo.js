function AddTask() {
  var TaskInformation = document.getElementById("TaskData").value
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "/AddTask", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("TaskInformation="+ TaskInformation);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var response = JSON.parse(xhttp.responseText);
      if (response["error"] === false){
          window.open("/", "_self");
      }

      else{
        if(response["reason"] == "ER_DUP_ENTRY"){
          alert("Duplicate Entry");
        }
        else{
            alert("Failed to Add Task!!");
        }

      }
    }
  };



}
function DeleteTask(TaskInformation) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "/DeleteTask", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("TaskInformation="+ TaskInformation);
  console.log("TaskInformation="+ TaskInformation);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var response = JSON.parse(xhttp.responseText);
      if (response["error"] === false){
          window.open("/", "_self");
      }
      else{
            alert("Failed to Delete Task!!");
        }
      }
    }


}

function UpdateTask(TaskInformation, IsCompleted) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "/UpdateTask", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("TaskInformation="+ TaskInformation + "&IsCompleted=" + IsCompleted);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var response = JSON.parse(xhttp.responseText);
      if (response["error"] === false){
          window.open("/", "_self");
      }
      else{
            alert("Failed to Update Task!!");
        }
      }
    }


}
