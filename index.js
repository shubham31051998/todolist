var express = require('express');
var app = express();
app.set('view engine', 'ejs');
app.use('/assets', express.static('assets'));
var mysql = require('mysql');
var bodyParser = require('body-parser');
// const config = require('./config.json')

const fs = require('fs')






var urlencodedParser = bodyParser.urlencoded({ extended: false })


var con




  


app.get('/', function(req, res){
  let rawdata = fs.readFileSync('config.json');
  let config = JSON.parse(rawdata);
  con = mysql.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database
  });
  con.connect(function(err) {
    if (err) {
      console.log("False ba")
      res.render('DatabaseConfig')
    }
    else{
      console.log("In true part")
      con.query("SELECT * FROM Task", function (err, result, fields) {
        if (err) throw err;
        var i, PendingTasks = [], CompletedTasks = [];

        for (i = 0; i < result.length; i++) {
          if(result[i].IsCompleted === 1){
            CompletedTasks.push(result[i].TaskInformation);
          }
          else{
            PendingTasks.push(result[i].TaskInformation);
          }
        }
        if(req.app.get("error") === undefined){
          req.app.set("error", "None");
        }
        console.log("In render")
        res.render('ToDo', {'CompletedTasks' : CompletedTasks, 'PendingTasks' : PendingTasks, 'Error' : req.app.get("error")});
      });
    }
  });
  
  
    


});

app.post('/DatabaseConfig', urlencodedParser, function(req, res){
  let config_data = { 
    host : req.body.host,
    user : req.body.user,
    password : req.body.password,
    database : req.body.database
  };
 
  let data = JSON.stringify(config_data);
  fs.writeFileSync('config.json', data);
  
  res.redirect("/");
});


app.post('/AddTask', urlencodedParser, function(req, res){
  var sql = "INSERT INTO Task(TaskInformation, IsCompleted) VALUES('" + req.body.TaskInformation +"', False)";
  
  con.query(sql, function (err, result) {
    var result = {};
    if (err){
      if(err.code === "ER_DUP_ENTRY"){
        req.app.set('error', "Duplicate Entry Not Allowed");
      }
      else{
        req.app.set('error', "Insert Failed");
      }
    }
    else{
      req.app.set('error', "None");
    }
    res.redirect("/");
  });
});

app.post('/DeleteTask', urlencodedParser, function(req, res){

  var sql = "DELETE FROM Task WHERE TaskInformation = '" + req.body.TaskInformation +"'";
  con.query(sql, function (err, result) {
    var result = {};
    if (err){
      req.app.set('error', "Delete Failed");
    }
    else{
      req.app.set('error', "None");
    }

    res.redirect("/");
  });
});

app.post('/UpdateTask', urlencodedParser, function(req, res){

  var sql = "UPDATE Task SET IsCompleted = !IsCompleted WHERE TaskInformation = '" + req.body.TaskInformation +"'";
  con.query(sql, function (err, result) {
    var result = {};
    if (err){
      req.app.set('error', "Update Failed");
    }
    else{
      req.app.set('error', "None");;
    }

    res.redirect("/");;
  });
});


app.listen(8200);
